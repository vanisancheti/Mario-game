import random
from Board import *
from obstacles import *
from person import *
import time,os
import globalvar
#1) tank 
#2) enemies
#3) coins
#4) planks
first_spawn=[0,0,0,0,0]
def spawn_tank():
	#first scene generation
	if(first_spawn[0]==0):
		while(len(globalvar.tank_array)<globalvar.tank_number):
			temp_point = [36,random.randrange(20,156)]
			actual_value = b1.screen[36:39,temp_point[1]-1:temp_point[1]+4]
			if( numpy.array_equal([[' ',' ',' ',' ',' '],[' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ']],actual_value) ):
				b1.screen[36:39,temp_point[1]:temp_point[1]+3] = tank_struct
				temp = tank(tank_struct,temp_point)
				globalvar.tank_array.append(temp)
		first_spawn[0]=1

	while(len(globalvar.tank_array)<globalvar.tank_number):
		temp_point = [36,random.randrange(160,246)]
		actual_value = b1.screen[36:39,temp_point[1]-1:temp_point[1]+4]

		#checking if that position is empty or not
		if( numpy.array_equal([[' ',' ',' ',' ',' '],[' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ']],actual_value) ):
			b1.screen[36:39,temp_point[1]:temp_point[1]+3] = tank_struct
			temp = tank(tank_struct,temp_point)
			globalvar.tank_array.append(temp)


def spawn_enemy():
	#First scene generation 
	if(first_spawn[1]==0):
		while(len(globalvar.enemy_array)<globalvar.enemy_number):
			temp_point = [37,random.randrange(20,157)]
			actual_value = b1.screen[37:39,temp_point[1]-1:temp_point[1]+3]
			if( numpy.array_equal([[' ',' ',' ',' '],[' ',' ',' ',' ']],actual_value) ):
				b1.screen[37:39,temp_point[1]:temp_point[1]+2] = enemy_struct
				type_movement = random.randrange(0,2)
				x = random.randrange(0,4) 
				if(x == 0):
					temp = Boss_enemy(benemy_struct,temp_point)
				else:
					temp = Enemies(enemy_struct,temp_point,type_movement)
				globalvar.enemy_array.append(temp)
		first_spawn[1]=1

	while(len(globalvar.enemy_array)<globalvar.enemy_number):
		temp_point = [37,random.randrange(175,247)]
		actual_value = b1.screen[37:39,temp_point[1]-1:temp_point[1]+3]

		#checking if that position is empty or not
		if( numpy.array_equal([[' ',' ',' ',' '],[' ',' ',' ',' ']],actual_value) ):
			b1.screen[37:39,temp_point[1]:temp_point[1]+2] = enemy_struct
			type_movement = random.randrange(0,2)
			x = random.randrange(0,4) 
			if(x == 0):
				temp = Boss_enemy(benemy_struct,temp_point)
			else:
				temp = Enemies(enemy_struct,temp_point,type_movement)
			globalvar.enemy_array.append(temp)


def spawn_coin():
	#first scene generation
	if(first_spawn[2]==0):
		while(len(globalvar.coin_array)<globalvar.coin_number):
			temp_point = [38,random.randrange(15,156)]
			actual_value = b1.screen[38:39,temp_point[1]-1:temp_point[1]+4]

			#checking if that position is empty or not
			if( numpy.array_equal([[' ',' ',' ',' ',' ']],actual_value) ):
				b1.screen[38:39,temp_point[1]:temp_point[1]+3] = coin_struct
				type_coin = random.randrange(0,4)
				if(type_coin==0):
					coins = Coin("gems",coin_struct,temp_point)
				else:	
					coins = Coin("coin",coin_struct,temp_point)
				globalvar.coin_array.append(coins)

		first_spawn[2]=1

	while(len(globalvar.coin_array)<globalvar.coin_number):
		temp_point = [38,random.randrange(160,246)]
		actual_value = b1.screen[38:39,temp_point[1]-1:temp_point[1]+4]

		#checking if that position is empty or not
		if( numpy.array_equal([[' ',' ',' ',' ',' ']],actual_value) ):
			b1.screen[38:39,temp_point[1]:temp_point[1]+3] = coin_struct
			type_coin = random.randrange(0,4)
			if(type_coin == 0):
				coins = Coin("gem",gem_struct,temp_point)
			else:
				coins = Coin("coin",coin_struct,temp_point)
			globalvar.coin_array.append(coins)


def spawn_plank():
	#first scene generation
	if(first_spawn[3]==0):
		while(len(globalvar.plank_array)<globalvar.plank_number):
			temp_point = [33,random.randrange(15,156)]
			actual_value = b1.screen[33:35,temp_point[1]-1:temp_point[1]+4]

			#checking if that position is empty or not
			if( numpy.array_equal([[' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ']],actual_value) ):
				b1.screen[33:35,temp_point[1]:temp_point[1]+3] = plank_struct
				planks = Plank(plank_struct,temp_point)
				globalvar.plank_array.append(planks)
		first_spawn[3]=1

	while(len(globalvar.plank_array)<globalvar.plank_number):
		temp_point = [33,random.randrange(160,246)]
		actual_value = b1.screen[33:35,temp_point[1]-1:temp_point[1]+4]

		#checking if that position is empty or not
		if( numpy.array_equal([[' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ']],actual_value) ):
			b1.screen[33:35,temp_point[1]:temp_point[1]+3] = plank_struct
			planks = Plank(plank_struct,temp_point)
			globalvar.plank_array.append(planks)


def spawn_bridge():
	#first scene generation
	if(first_spawn[4]==0):
		while(len(globalvar.bridge_array)<globalvar.bridge_number):
			temp_point = [36,random.randrange(15,149)]
			actual_value = b1.screen[36:39,temp_point[1]:temp_point[1]+10]

			#checking if that position is empty or not
			if( numpy.array_equal([[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']],actual_value) ):
				b1.screen[36:39,temp_point[1]:temp_point[1]+10] = bridge_struct
				bridges = Bridges(bridge_struct,temp_point)
				globalvar.bridge_array.append(bridges)
		first_spawn[4]=1

	while(len(globalvar.bridge_array)<3):
		temp_point = [36,random.randrange(160,239)]
		actual_value = b1.screen[36:39,temp_point[1]-1:temp_point[1]+9]

		#checking if that position is empty or not
		if( numpy.array_equal([[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']],actual_value) ):
			b1.screen[36:39,temp_point[1]:temp_point[1]+10] = bridge_struct
			bridges = Bridges(bridge_struct,temp_point)
			globalvar.bridge_array.append(bridges)
			