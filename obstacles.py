import random
import globalvar
from Board import *
class obstacles():
	def __init__(self,type,pos,point):
		self.__type = type
		self.__pos = pos
		self.point = point
	
	def getType(self):
		return self.__type

	def getPos(self):
		return self.__pos

	def moveleft(self):
		pass

	def moveright(self):
		pass

class tank(obstacles):
	def __init__(self,pos,point):
		obstacles.__init__(self,"tank",pos,point)

	def moveleft(self):
		b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+3] = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
		if(self.point[1]>1):
			self.point[1]-=1
			b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+3] = self.getPos()
		else:
			globalvar.tank_array.remove(self)

	def moveright(self):
		b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+3] = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
		if(self.point[1]<=198):
			self.point[1]+=1
			b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+3] = self.getPos()

class Plank(obstacles):
	def __init__(self,pos,point):
		obstacles.__init__(self,"plank",pos,point)

	def moveleft(self):
		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+3] = [[' ',' ',' '],[' ',' ',' ']]
		if(self.point[1]>1):
			self.point[1]-=1
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+3] = self.getPos()

	def moveright(self):
		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+3] = [[' ',' ',' '],[' ',' ',' ']]
		if(self.point[1]!=0):
			self.point[1]+=1
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+3] = self.getPos()

class Coin(obstacles):
	def __init__(self,type,pos,point):
		obstacles.__init__(self,type,pos,point)

	def moveleft(self):
		b1.screen[self.point[0]:self.point[0]+1,self.point[1]:self.point[1]+3] = [[' ',' ',' ']]
		if(self.point[1]>1):
			self.point[1]-=1
			b1.screen[self.point[0]:self.point[0]+1,self.point[1]:self.point[1]+3] = self.getPos()
		else:
			globalvar.coin_array.remove(self)

	def moveright(self):
		b1.screen[self.point[0]:self.point[0]+1,self.point[1]:self.point[1]+3] = [[' ',' ',' ']]
		if(self.point[1]<=198):
			self.point[1]+=1
			b1.screen[self.point[0]:self.point[0]+1,self.point[1]:self.point[1]+3] = self.getPos()


class Bridges(obstacles):
	def __init__(self,pos,point):
		obstacles.__init__(self,"bridge",pos,point)

	def moveleft(self):
		b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+10] = [[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']]
		if(self.point[1]>1):
			self.point[1]-=1
			b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+10] = self.getPos()
		else:
			globalvar.bridge_array.remove(self)

	def moveright(self):
		b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+10] = [[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']] 
		if(self.point[1]<=198):
			self.point[1]+=1
			b1.screen[self.point[0]:self.point[0]+3,self.point[1]:self.point[1]+10] = self.getPos()

#struct of the obstacles
tank_struct = [['|','|','|'],['|','|','|'],['|','|','|']]
plank_struct = [['-','-','-'],['-','-','-']]
coin_struct = [['(','$',')']]
gem_struct = [['(','*',')']]
bridge_struct =[['_','_','_','-',' ',' ','-','_','_','_'],['|','*','*',' ',' ',' ',' ','*','*','|'],['|','*','*','*',' ',' ','*','*','*','|']]

