# Mario-Game (A Terminal based Game)

###  Instructions for running the 
--------------------------------------------------------------------------------

> - Create a Virtualenv
> - Install all the module listed in the requirement.txt
> - To start the Game run "python3 main.py"
> - To move the mario left Press -- 'a'
> - To move the mario right Press -- 'd'
> - To make the mario jump Press -- 'w' 

--------------------------------------------------------------------------------

## About the Files

# Board.py
Board is a matrix of of 50 X 250 . 
Contains all the functions that are related to the Board such as 
__init() :: Used for initializing the Board
printmatrix :: For printing the screen

## globalvar.py
This file contains all the global variables that are used in this game

## main.py
This file is run to start the game. Its takes the input from the user if given and 
execute the functionality accordingly.

## obstacles.py
It contains all the obstacle classes such as coins ,bridges ,tanks ,planks .
These classes are inherited from the obstacle class.These subclasses override some 
functions from the parent class as needed as well as creates some functions if required

## person.py
It contains a parent class named Person and two subclasses which are Mario and the Enemies.
Enemies have a subclass named Boss_enemy which is a special type of enemy
It contains all the functionality for the movements of the mario, the enemies and the boss enemy.
It contains commands for changing the background as the mario moves,for collecting the coins and gems,
for removing a enemy if the mario kills it and decresing the lives of the mario if a enemy attacks it.

## requirements.txt
It listed all the additional modules that are required

## spawn.py
It generates new scene as the background is moving toward left when the mario moves toward right.
It randomly generates all the obstacles :: coins,planks,bridges,tanks and enemies 
Coins are generated in which the gem type of coins are generated less frequently as the normal coins are.(with only (1/4)th probability)
Similarly the boss enemy  are generated with (1/4)th probability.

## starting.py
It contains all the scenes for different phases of the game
Starting scene: Shown when the game is started 
Level scene: Shown when next level is attained
Ending scene: Shown when the games gets over

# Features
The game starts with a starting scene where the user is asked to press the spacebar to start the game.If the user dosen't 
starts the game for 10 sec the game would end.

Enemies and all the obstacles are randomly generated.

Display of score ,lives and levels are there in every scene.


The Level of the game change as the score reaches a particular value(Levels are upto 3 only)
Next Level contains more number of enemies,coins,planks,tanks

Coins are of two different type :
Noramal coins increases the score by one
Gems increses the score by two

A bridge is implemented which contains a hole if the mario falls into the hole the game would come to a end

Enemies are of two different types:
Normal enemies : decreases the lives of the mario by one and gets killed by the mario if the jumps upon it
Boss enemies : it moves according to the mario position (chases the mario), diffucult for the mario to kill it 
                as if the boos enemy touches the mario ,mario id dead
                
Enemies firstly move in one direction(randomly decided), if it get stuck in that direction then its moving in the other direction 

Bonus points are awarded after each one minute

If the mario moves out of the window it is replaced to the middle of the window ,and according the background also moves.

While the game goes on , sound is played at the begining ,ending ,changing of the levels,jumping ,collecting coins,killing a enemy,decrease of the mario lives.
