from Board import *
import globalvar
# "WELCOME TO THE MARIO-GAME"  
# "INSTRUCTION:"
	# "MARIO-MOVEMENTS ::"
		# "Press key 'd' for moving the mario to the left"
		# "Press key 'a' for moving the mario to the right"
		# "Press key 'w' for making mario jump"
# "Press Enter for starting the game"


# starting scene
starting = Board()

welcome = [['#',' ',' ',' ','#',' ','#','#','#',' ','#',' ',' ',' ','#','#','#',' ','#','#','#',' ','#','#',' ','#','#',' ','#','#','#'],
		   ['#',' ',' ',' ','#',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ','#',' ','#','#',' ','#','#',' ','#',' ',' '],
		   ['#',' ','#',' ','#',' ','#','#','#',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ','#',' ','#',' ','#',' ','#',' ','#','#','#'],
		   ['#','#',' ','#','#',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ','#',' ','#',' ',' ',' ','#',' ','#',' ',' '],
		   ['#','#',' ','#','#',' ','#','#','#',' ','#','#','#',' ','#','#','#',' ','#','#','#',' ','#',' ',' ',' ','#',' ','#','#','#']]

starting.screen[10:15,65:96] = welcome

statement = []
for i in str("INSTRUCTION:: "):
	statement.append(i)
starting.screen[17:18,35:35+len(statement)]  = statement
statement = []
for i in str("MARIO-MOVEMENTS:: "):
	statement.append(i)
starting.screen[18:19,40:40+len(statement)]  = statement
statement = []
for i in str("Press key 'd' for moving the mario to the left"):
	statement.append(i)
starting.screen[19:20,43:43+len(statement)]  = statement
statement = []
for i in str("Press key 'a' for moving the mario to the right"):
	statement.append(i)
starting.screen[20:21,43:43+len(statement)]  = statement
statement = []
for i in str("Press key 'w' for making mario jump"):
	statement.append(i)
starting.screen[21:22,43:43+len(statement)]  = statement
statement = []
for i in str("Press SpaceBar for starting the game"):
	statement.append(i)
starting.screen[22:23,43:43+len(statement)]  = statement

#level scene
level = Board()

level_change = [['#',' ',' ',' ','#',' ','#','#','#',' ','#',' ','#',' ','#','#','#',' ',' ','#',' ',' ',' ','#','#','#',' ','#',' ','#',' ','#','#','#',' ','#',' ',' '],
				['#','#',' ',' ','#',' ','#',' ',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ','#',' ','#',' ',' ',' ','#',' ',' '],
				['#',' ','#',' ','#',' ','#','#','#',' ',' ',' ',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#','#','#',' ','#',' ','#',' ','#','#','#',' ','#',' ',' '],
				['#',' ',' ','#','#',' ','#',' ',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ','#',' ','#',' ','#',' ',' ',' ','#',' ',' '],
				['#',' ',' ',' ','#',' ','#','#','#',' ','#',' ','#',' ',' ','#',' ',' ',' ','#','#','#',' ','#','#','#',' ',' ','#',' ',' ','#','#','#',' ','#','#','#']]

level.screen[25:30,65:103] = level_change
#ending scene
ending = Board()

game_over = [['#','#','#',' ','#','#','#',' ','#','#',' ','#','#',' ','#','#','#',' ',' ','#','#','#',' ','#',' ','#',' ','#','#','#',' ','#','#',' '],
			 ['#',' ',' ',' ','#',' ','#',' ','#','#',' ','#','#',' ','#',' ',' ',' ',' ','#',' ','#',' ','#',' ','#',' ','#',' ',' ',' ','#',' ','#'],
			 ['#','#','#',' ','#','#','#',' ','#',' ','#',' ','#',' ','#','#','#',' ',' ','#',' ','#',' ','#',' ','#',' ','#','#','#',' ','#','#',' '],
			 ['#',' ','#',' ','#',' ','#',' ','#',' ',' ',' ','#',' ','#',' ',' ',' ',' ','#',' ','#',' ','#',' ','#',' ','#',' ',' ',' ','#',' ','#'],
			 ['#','#','#',' ','#',' ','#',' ','#',' ',' ',' ','#',' ','#','#','#',' ',' ','#','#','#',' ',' ','#',' ',' ','#','#','#',' ','#',' ','#']]

ending.screen[25:30,65:99] = game_over

statement = []
for i in str("SCORE:: "):
	statement.append(i)
ending.screen[40:41,65:65+len(statement)]  = statement

