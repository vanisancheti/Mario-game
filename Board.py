import numpy,os
import globalvar
from colorama import Fore,Back,Style,init
init(autoreset=True)

class Board():
	def __init__(self):
		self.screen = [' ']*50
		for i in range(50):
			self.screen[i] = [' ']*250

		#Setting up the boundaries
		self.screen[0] = ['#']*159 + [' ']*91
		self.screen[49] = ['#']*159 + [' ']*91
		self.screen[39] = ['#']*159 + [' ']*91

		for i in range(50):
			self.screen[i][0] = '#'
			self.screen[i][159] = '#'

		self.screen = numpy.reshape(self.screen,(50,250))

	def printmatrix(self):
		os.system('clear')

		#Setting up the boundaries
		self.screen[0] = ['#']*159 + [' ']*91
		self.screen[49] = ['#']*159 + [' ']*91
		self.screen[39] = ['#']*159 + [' ']*91

		for i in range(50):
			self.screen[i][0] = '#'
			self.screen[i][159] = '#'

		row_no=0

		# displaying a hole inbetween the bridge according to the location of it
		breakvalue=[]
		for i in globalvar.bridge_array:
			breakvalue.append(i.point[1]+4)
			breakvalue.sort()

		for i in breakvalue:
			self.screen[39][i]=' '
			self.screen[39][i+1] = ' '

		# different parts of the screen 
		for row in self.screen:
			row = row[0:160]
			if(row_no>=39):
				x=0
				for i in breakvalue:
					row1 = row[x:i]
					print(Back.BLACK+"".join(map(str,row1)),end='')
					print("".join(map(str,row[i:i+2])),end='')
					x=i+2
				row1 = row[x:160]
				print(Back.BLACK+"".join(map(str,row1)))
			else:
				if(row_no<=15):
					print(Back.CYAN+"".join(map(str,row)))
				else:
					print(Back.GREEN+"".join(map(str,row)))
			row_no+=1
b1 = Board()


