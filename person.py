import time
import random
from Board import *
from obstacles import *
import fcntl,sys,tty,termios

class person():
	def __init__(self,type,pos,point):
		self.__type = type
		self.__pos = pos
		self.point = point

	def getType(self):
		return self.__type

	def getPos(self):
		return self.__pos

class Mario(person):
	def __init__(self,pos,point):
		person.__init__(self,"mario",pos,point)

	def checkstability(self):
		while(self.point[0]!=49): #falling down if at a height above the surface
			a = b1.screen[self.point[0]+2:self.point[0]+3,self.point[1]:self.point[1]+2]
			if(numpy.array_equal(a,[[' ',' ']])):
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
				self.point[0]+=1
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
			else:
				break			

	def moveleft(self):
		#move left
		if(self.point[1]!=1):
			while(self.point[0]!=49):#if moving from the top of an obstacle
				a = b1.screen[self.point[0]+2:self.point[0]+3,self.point[1]:self.point[1]+2]
				if(numpy.array_equal(a,[[' ',' ']])):
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
					self.point[0]+=1
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
				else:
					break;
			#checking for overlapping
			a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]-1:self.point[1]] 
			if(numpy.array_equal(a,[[' '],[' ']])):
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
				self.point[1]-=1
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
			else:
				a = b1.screen[self.point[0]+1:self.point[0]+2,self.point[1]-3:self.point[1]]
				if(numpy.array_equal(a,[['(','$',')']]) or numpy.array_equal(a,[['(','*',')']])):
					globalvar.Score+=1
					for i in globalvar.coin_array:
						if(i.point[1]==self.point[1]-3):
							b1.screen[self.point[0]+1:self.point[0]+2,self.point[1]-3:self.point[1]] = [[' ',' ',' ']]
							globalvar.coin_array.remove(i)
							self.moveleft()

	def moveright(self):
		#move right
		if(self.point[1]<156):
			while(self.point[0]!=49): #falling down if at a height above the surface
				a = b1.screen[self.point[0]+2:self.point[0]+3,self.point[1]:self.point[1]+2]
				if(numpy.array_equal(a,[[' ',' ']])):
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
					self.point[0]+=1
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
				else:
					break	

			#checking for overlapping		
			a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]+2:self.point[1]+4] 
			if(numpy.array_equal(a,[[' ',' '],[' ',' ']])):
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
				self.point[1]+=1
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
				#moving surrounding accordingly
				for i in globalvar.tank_array:
					i.moveleft()

				for i in globalvar.enemy_array:
					if(i.getType() == "Boss_enemy"):
						b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+2] = [[' ',' '],[' ',' ']]
						i.point[1]-=1
						if(i.point[1]<=0):
							globalvar.enemy_array.remove(i)
						else:
							b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+2] = i.getPos()
				for i in globalvar.coin_array:
					i.moveleft()

				for i in globalvar.plank_array:
					i.moveleft()

				for i in globalvar.bridge_array:
					i.moveleft()
			else:
				#collecting coins and gems

				a = b1.screen[self.point[0]+1:self.point[0]+2,self.point[1]+2:self.point[1]+5]
				if(numpy.array_equal(a,[['(','$',')']]) or numpy.array_equal(a,[['(','*',')']])):
					for i in globalvar.coin_array:
						if(i.point[1]==self.point[1]+2):
							b1.screen[self.point[0]+1:self.point[0]+2,self.point[1]+2:self.point[1]+5] = [[' ',' ',' ']]
							if(i.getType()=='coin'):
								globalvar.Score+=1
							else:
								globalvar.Score+=2
							os.system('aplay -q sounds/coin.wav&')
							globalvar.coin_array.remove(i)
							self.moveright()

				if(numpy.array_equal(a,[[' ','(','$']]) or numpy.array_equal(a,[[' ','(','*']])):
					globalvar.Score+=1
					for i in globalvar.coin_array:
						if(i.point[1]==self.point[1]+3):
							b1.screen[self.point[0]+1:self.point[0]+2,self.point[1]+3:self.point[1]+6] = [[' ',' ',' ']]
							if(i.getType() == "coin"):
								globalvar.Score+=1
							else:
								globalvar.Score+=2
							os.system('aplay -q sounds/coin.wav&')	
							globalvar.coin_array.remove(i)
							self.moveright()


		else:
			#moving the mario to the center of the screen if it goes to the end of the screen
			#and accordingly moving the surrounding obstacles
			#all the obstacles that are placed in position whose column number are less than 78 are deleted first than the rest of the obstacles are moved to avoid overlapping

			if(self.point[1]>=156):
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
				for i in globalvar.tank_array:
					b1.screen[i.point[0]:i.point[0]+3,i.point[1]:i.point[1]+3] = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]	
					if(i.point[1]<=78):
						globalvar.tank_array.remove(i)

				for i in globalvar.coin_array:
					b1.screen[i.point[0]:i.point[0]+1,i.point[1]:i.point[1]+3] = [[' ',' ',' ']]
					if(i.point[1]<=78):
						globalvar.coin_array.remove(i)

				for i in globalvar.enemy_array:
					b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+2] = [[' ',' '],[' ',' ']]
					if(i.point[1]<=78):
						globalvar.enemy_array.remove(i)


				for i in globalvar.plank_array:
					b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+3] = [[' ',' ',' '],[' ',' ',' ']]
					if(i.point[1]<=78):
						globalvar.plank_array.remove(i)

				for i in globalvar.bridge_array:	
					b1.screen[i.point[0]:i.point[0]+3,i.point[1]:i.point[1]+10] = [[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']]	
					if(i.point[1]<=78):
						globalvar.bridge_array.remove(i)

				for i in globalvar.tank_array:
					b1.screen[i.point[0]:i.point[0]+3,i.point[1]:i.point[1]+3] = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
					i.point[1]-=78
					if(i.point[1]<=0):
						globalvar.tank_array.remove(i)
					else:
						b1.screen[i.point[0]:i.point[0]+3,i.point[1]:i.point[1]+3] = tank_struct

				for i in globalvar.coin_array:
					b1.screen[i.point[0]:i.point[0]+1,i.point[1]:i.point[1]+3] = [[' ',' ',' ']]
					i.point[1]-=78
					if(i.point[1]<=0):
						globalvar.coin_array.remove(i)
					else:
						b1.screen[i.point[0]:i.point[0]+1,i.point[1]:i.point[1]+3] = coin_struct

				for i in globalvar.enemy_array:
					b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+2] = [[' ',' '],[' ',' ']]
					i.point[1]-=78
					if(i.point[1]<=0):
						globalvar.enemy_array.remove(i)
					else:
						b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+2] = enemy_struct

				for i in globalvar.plank_array:
					b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+3] = [[' ',' ',' '],[' ',' ',' ']]
					i.point[1]-=78
					if(i.point[1]<=0):
						globalvar.plank_array.remove(i)
					else:
						b1.screen[i.point[0]:i.point[0]+2,i.point[1]:i.point[1]+3] = plank_struct

				for i in globalvar.bridge_array:
					b1.screen[i.point[0]:i.point[0]+3,i.point[1]:i.point[1]+10] = [[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']]
					i.point[1]-=78
					if(i.point[1]<=0):
						globalvar.bridge_array.remove(i)
					else:
						b1.screen[i.point[0]:i.point[0]+3,i.point[1]:i.point[1]+10] = bridge_struct
					
				self.point[1]-=78
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
				self.moveright()
				b1.printmatrix()


	def jump(self,jump_direction):
		#jump
		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
		if(jump_direction=='d'):
			point_curry = self.point[1]+5
		else:
			if(jump_direction=='a'):
				point_curry = self.point[1]-5
			else:
				point_curry = self.point[1]

		#moving the mario upwards checking at every point that it dosen't collide
		point_currx = self.point[0]-7
		while(self.point[0]!=point_currx):
			a = b1.screen[self.point[0]-1:self.point[0],self.point[1]:self.point[1]+2]
			if(numpy.array_equal(a,[[' ',' ']])):
				self.point[0]-=1
				os.system('aplay -q sounds/jump.wav&')
				# os.sys.exit()
				# x-=1
			else:
				break;


		#moving the mario towards right checking at every point that it doesn't collide
		while(self.point[1]!=point_curry):
			a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]+2:self.point[1]+4] 
			if(numpy.array_equal(a,[[' ',' '],[' ',' ']])):
				if(jump_direction=='d'):
					self.point[1]+=1
				else:
					self.point[1]-=1
			else:
				break

		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
		b1.printmatrix()

		time.sleep(.1)#to bring an effect of jump
			

		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
		if(jump_direction=='d'):
			point_curry = self.point[1]+5
		else:
			if(jump_direction=='a'):
				point_curry = self.point[1]-5
			else:
				point_curry = self.point[1]

		#moving the mario towards right checking at every point that it doesn't collide
		while(self.point[1]!=point_curry):
			a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]+2:self.point[1]+4] 
			if(numpy.array_equal(a,[[' ',' '],[' ',' ']])):
				if(jump_direction=='d'):
					self.point[1]+=1
				else:
					self.point[1]-=1
			else:
				break

		#moving the mario downwards checking at every point that it dosen't collide

		while(self.point[0]!=49):
			a = b1.screen[self.point[0]+2:self.point[0]+3,self.point[1]:self.point[1]+3]
			if(numpy.array_equal(a,[[' ',' ',' ']])):
				self.point[0]+=1
			else:
				if(numpy.array_equal(a,[['(','$',')']]) or numpy.array_equal(a,[['(','*',')']])):
					for i in globalvar.coin_array:
						if(i.point[1]==self.point[1]):
							b1.screen[self.point[0]+2:self.point[0]+3,self.point[1]:self.point[1]+3] = [[' ',' ',' ']]
							if(i.getType()=="coin"):
								globalvar.Score+=1
							else:
								globalvar.Score+=2
							os.system('aplay -q sounds/coin.wav&')
							globalvar.coin_array.remove(i)
							self.moveright()

				
				if(numpy.array_equal(a,[[' ','(','$']]) or numpy.array_equal(a,[[' ','(','*']])):
					for i in globalvar.coin_array:
						if(i.point[1]==self.point[1]+1):
							b1.screen[self.point[0]+2:self.point[0]+3,self.point[1]+1:self.point[1]+4] = [[' ',' ',' ']]
							if(i.getType()=="gem"):
								globalvar.Score+=2
							else:
								globalvar.Score+=1
							os.system('aplay -q sounds/coin.wav&')
							globalvar.coin_array.remove(i)
							self.moveright()		
				break;

		#moving the surrounding accordingly
		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()

class Enemies(person):
	def __init__(self,pos,point,move,type = "enemy"):
		person.__init__(self,type,pos,point)
		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
		self.ctime = 0
		self.ptime = 0
		self.movement = move

	def moveleft(self):
		#collision with the mario on top will kill the enemy
		if(self.point[0]==mario.point[0]+2 and self.point[1]==mario.point[1]):
			os.system('aplay -q sounds/enemytouch.wav&')
			globalvar.enemy_array.remove(self)
			globalvar.Score+=2#increase the score by 2 for killing a enemy
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
			mario.moveright()
			return 
		#if the enemy moves out of the screen remove the enemy
		if(self.point[1]<=1):
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
			globalvar.enemy_array.remove(self)
			return

		#checking that the movement doesn't cause any overlapping 
		a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]-2:self.point[1]]
		if( numpy.array_equal([[' ',' '],[' ',' ']],a) == True or numpy.array_equal([['#',' '],['#',' ']],a) == True):
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
			self.point[1]-=1
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] =self.getPos()
		else:
			#if one side is blocked the enemy starts moving the other side
			if(numpy.array_equal([['M',' '],['M',' ']],a) == True):
				os.system('aplay -q sounds/livesdecreases.wav&')
				globalvar.Lives-=1

			self.movement = (self.movement+1)%2

	def moveright(self):
		#collision with the mario on top will kill the enemy
		if(self.point[0]==mario.point[0]+2 and self.point[1]==mario.point[1]):
			os.system('aplay -q sounds/enemytouch.wav&')
			globalvar.enemy_array.remove(self)
			globalvar.Score+=2#increase the score by 2 for killing a enemy
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
			mario.moveleft()
			return 

		#checking that the movement doesn't cause any overlapping	
		a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]+2:self.point[1]+4]
		if(numpy.array_equal([[' ',' '],[' ',' ']],a) == True):
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
			self.point[1]+=1
			b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] =self.getPos()
		else:
			#if one side is blocked than the enemy starts moving the other side
			if(numpy.array_equal([[' ','M'],[' ','M']],a) == True):
				os.system('aplay -q sounds/livesdecreases.wav&')
				globalvar.Lives-=1

			self.movement = (self.movement+1)%2


class Boss_enemy(Enemies):
	def __init__(self,pos,point):
		Enemies.__init__(self,pos,point,0,"Boss_enemy")
		self.live = 2
		b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = pos

	def movements(self):
		#collision with the mario on top will kill the enemy
		if(self.point[0]==mario.point[0]+2 and self.point[1]==mario.point[1]):
			self.live-=1
			globalvar.Score+=4#increase the score by 4 for decreasing the strength of a boss enemy
			
			mario.moveright()
			if(self.live<=0):
				b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
				globalvar.enemy_array.remove(self)	
			return 

		if(self.point[1]-mario.point[1]>0):
			a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]-2:self.point[1]]
			if(self.point[1]>0):
				if(numpy.array_equal(a,[[' ',' '],[' ',' ']]) or numpy.array_equal([['#',' '],['#',' ']],a)):
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
					self.point[1]-=1
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
				else:
					if(numpy.array_equal([['M',' '],['M',' ']],a) or numpy.array_equal([['M','M'],['M','M']],a) ):
						os.system('aplay -q sounds/livesdecreases.wav&')
						globalvar.Lives-=1
						mario.moveleft()
		else:
			a = b1.screen[self.point[0]:self.point[0]+2,self.point[1]+2:self.point[1]+4]
			if(self.point[1]>0):
				if(numpy.array_equal(a,[[' ',' '],[' ',' ']])):
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = [[' ',' '],[' ',' ']]
					self.point[1]+=1
					b1.screen[self.point[0]:self.point[0]+2,self.point[1]:self.point[1]+2] = self.getPos()
				else:
					if(numpy.array_equal([[' ','M'],[' ','M']],a) or numpy.array_equal([['M','M'],['M','M']],a) ):
						os.system('aplay -q sounds/livesdecreases.wav&')
						globalvar.Lives-=1
						mario.moveright()
						mario.moveright()

# Structure of the mario and the enemy
mario_struct = [['M','M'],['M','M']]
enemy_struct = [['E','E'],['E','E']]
benemy_struct = [['o','o'],['O','O']]

#starting point of the mario
point_mario = [37,1]

#Initializing the mario
b1.screen[37:39,1:3] = mario_struct
mario = Mario(mario_struct,point_mario)
