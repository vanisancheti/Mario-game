import time,random
import globalvar 
from Board import *
from person import *
from obstacles import *
from spawn import *
from starting import *
import fcntl,sys,tty,termios
class raw(object):
    def __init__(self, stream):
        self.stream = stream
        self.fd = self.stream.fileno()
    def __enter__(self):
        self.original_stty = termios.tcgetattr(self.stream)
        tty.setcbreak(self.stream)
    def __exit__(self, type, value, traceback):
        termios.tcsetattr(self.stream, termios.TCSANOW, self.original_stty)

class nonblocking(object):
    def __init__(self, stream):
        self.stream = stream
        self.fd = self.stream.fileno()
    def __enter__(self):
        self.orig_fl = fcntl.fcntl(self.fd, fcntl.F_GETFL)
        fcntl.fcntl(self.fd, fcntl.F_SETFL, self.orig_fl | os.O_NONBLOCK)
    def __exit__(self, *args):
        fcntl.fcntl(self.fd, fcntl.F_SETFL, self.orig_fl)


with raw(sys.stdin):
    with nonblocking(sys.stdin):
        #showing the starting window
        starting.printmatrix()

        #The game would end if the user didn't started in 10 sec
        curtime = time.time()
        char = 'enter'
        while(time.time()-curtime <=10):
            char = sys.stdin.read(1)
            if(char==' '):
                os.system('aplay -q sounds/starting.wav&')
                time.sleep(6)
                globalvar.started_time = time.time()
                while True:
                    # Bonus point after every 60 sec 
                    globalvar.current_time = time.time()
                    if(globalvar.current_time - globalvar.started_time > 60):
                        globalvar.Score+=15
                        globalvar.started_time = globalvar.current_time


                    try:
                        char = sys.stdin.read(1)
                        #movement of the mario according to the input if any
                        if(char=="q"):
                            os.system('aplay -q sounds/gameover.wav&')
                            show = []
                            globalvar.bridge_array = []
                            for i in str(globalvar.Score) :
                                show.append(i)
                                ending.screen[40:41,75:75+len(show)] = show
                            ending.printmatrix()
                            os.sys.exit()
                        else:
                            if(char=='a'):
                                mario.moveleft()

                            else:
                                if(char == 'd'):
                                    mario.moveright()
                                else:
                                    if(char=='w'):
                                        time.sleep(0.2)
                                        char1 = sys.stdin.read(1)
                                        mario.jump(char1)

                        #spawn different elements of the game
                        spawn_tank()
                        spawn_plank()
                        spawn_enemy()
                        spawn_coin()
                        spawn_bridge()

                        #movements of the enemy
                        for i in globalvar.enemy_array:
                            if(i.getType()=="Boss_enemy"):
                                i.movements()
                            else:
                                if(i.movement==0):
                                    i.moveleft()
                                else:
                                    i.moveright()
                        
                        

                        mario.checkstability()

                        #finializing the score
                        show = []
                        for i in str("SCORE"):
                            show.append(i)
                        b1.screen[10:11,130:135] = show

                        show=[]
                        for i in str("LIVES"):
                            show.append(i)
                        b1.screen[8:9,130:135] = show
                        
                        show = []
                        for i in str(globalvar.Score) :
                            show.append(i)

                        b1.screen[10:11,140:140+len(show)] = show

                        show = []
                        for i in str(globalvar.Lives):
                            show.append(i)
                        b1.screen[8:9,140:140+len(show)] = show

                        statement = []
                        for i in str("LEVEL"):
                            statement.append(i)
                        b1.screen[6:7,130:130+len(statement)]  = statement

                        statement = []
                        for i in str(globalvar.Levels):
                            statement.append(i)
                        b1.screen[6:7,140:140+len(statement)]  = statement

                        #final printing of the screen after setting the position of all elements of the game
                        b1.printmatrix()

                        if(globalvar.Lives <= 0):
                            time.sleep(2)
                            globalvar.bridge_array = []
                            show = []
                            for i in str(globalvar.Score) :
                                show.append(i)
                                ending.screen[40:41,75:75+len(show)] = show
                            ending.printmatrix()
                            os.system('aplay -q sounds/gameover.wav&')
                            os.sys.exit()

                        # different levels
                        x = globalvar.level_score_limit*10 
                        if(globalvar.Score > x and globalvar.Levels<3):
                            globalvar.Levels+=1
                            os.system('aplay -q sounds/nextlevel.wav&')
                            level.printmatrix()
                            time.sleep(1)
                            b1.printmatrix()
                            time.sleep(1)
                            globalvar.level_score_limit+=1
                            x+=globalvar.level_score_limit*10
                            globalvar.plank_number+=4
                            globalvar.enemy_number+=2
                            globalvar.tank_number+=1
                            globalvar.coin_number+=2

                        #mario falls in the well
                        if(mario.point[0]==47):
                            os.system('aplay -q sounds/gameover.wav&')  
                            globalvar.bridge_array = []
                            show = []
                            for i in str(globalvar.Score) :
                                show.append(i)
                            ending.screen[40:41,75:75+len(show)] = show
                            ending.printmatrix()
                            time.sleep(1)
                            os.sys.exit()                        

                    except IOError:
                        print('not ready')
                    time.sleep(.1)
